package com.ray.auth.mapper;

import org.springframework.stereotype.Repository;

import com.ray.auth.model.UserRole;
import com.ray.framework.mybatis.mapper.BaseMapper;

/**
 * 用户角色Mapper
 */
@Repository
public interface UserRoleMapper extends BaseMapper<String, UserRole> {

}
