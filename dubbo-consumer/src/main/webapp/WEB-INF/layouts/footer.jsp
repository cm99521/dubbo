<%@ page language="java" pageEncoding="UTF-8"%>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<div class="ui inverted vertical footer segment">
	<div class="ui center aligned container">
		<div class="ui stackable inverted divided grid">
			<div class="three wide column">
				<h4 class="ui inverted header">友情链接</h4>
				<div class="ui inverted link list">
					<a href="http://www.csdn.net" class="item">CSDN</a> <a href="http://www.oschina.net" class="item">开源中国</a> <a href="http://www.zhihu.com" class="item">知乎</a> <a href="http://www.baidu.com" class="item">百度</a>
				</div>
			</div>
			<div class="three wide column">
				<h4 class="ui inverted header">友情链接</h4>
				<div class="ui inverted link list">
					<a href="http://www.csdn.net" class="item">CSDN</a> <a href="http://www.oschina.net" class="item">开源中国</a> <a href="http://www.zhihu.com" class="item">知乎</a> <a href="http://www.baidu.com" class="item">百度</a>
				</div>
			</div>
			<div class="three wide column">
				<h4 class="ui inverted header">友情链接</h4>
				<div class="ui inverted link list">
					<a href="http://www.csdn.net" class="item">CSDN</a> <a href="http://www.oschina.net" class="item">开源中国</a> <a href="http://www.zhihu.com" class="item">知乎</a> <a href="http://www.baidu.com" class="item">百度</a>
				</div>
			</div>
			<div class="seven wide column">
				<h4 class="ui inverted header">后台管理系统</h4>
				<p>一个简单的示例系统</p>
			</div>
		</div>
		<div class="ui inverted section divider"></div>
		<img src="${ctx}/static/images/logo.png" class="ui centered mini image">
		<div class="ui horizontal inverted small divided link list">
			<a class="item" href="#">公司地址</a> <a class="item" href="#">关于我们</a> <a class="item" href="#">公司介绍</a>
		</div>
	</div>
</div>